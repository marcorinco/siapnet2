﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmPrincipal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmPrincipal))
        Me.pnlBarra = New System.Windows.Forms.Panel()
        Me.btnPerfil = New FontAwesome.Sharp.IconButton()
        Me.btnMaximizar = New FontAwesome.Sharp.IconButton()
        Me.btnRestaurar = New FontAwesome.Sharp.IconButton()
        Me.btnMinimizar = New FontAwesome.Sharp.IconButton()
        Me.btnCerrar = New FontAwesome.Sharp.IconButton()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.pnlStatus = New System.Windows.Forms.Panel()
        Me.pnlMenu = New System.Windows.Forms.Panel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.btnHome = New FontAwesome.Sharp.IconButton()
        Me.btnSalir = New FontAwesome.Sharp.IconButton()
        Me.pnlSistemas = New System.Windows.Forms.Panel()
        Me.btnSisResp = New FontAwesome.Sharp.IconButton()
        Me.btnSisEjCon = New FontAwesome.Sharp.IconButton()
        Me.btnSisEjCo = New FontAwesome.Sharp.IconButton()
        Me.btnSisEjAc = New FontAwesome.Sharp.IconButton()
        Me.btnSisImpF = New FontAwesome.Sharp.IconButton()
        Me.btnSisPara = New FontAwesome.Sharp.IconButton()
        Me.btnSisOpcS = New FontAwesome.Sharp.IconButton()
        Me.btnSisPfac = New FontAwesome.Sharp.IconButton()
        Me.btnSisCEmp = New FontAwesome.Sharp.IconButton()
        Me.btnSisEmp = New FontAwesome.Sharp.IconButton()
        Me.btnSisCla = New FontAwesome.Sharp.IconButton()
        Me.btnSisUsr = New FontAwesome.Sharp.IconButton()
        Me.btnSistema = New FontAwesome.Sharp.IconButton()
        Me.pnlReportes = New System.Windows.Forms.Panel()
        Me.btnRepAdm = New FontAwesome.Sharp.IconButton()
        Me.btnRepInv = New FontAwesome.Sharp.IconButton()
        Me.btnRepCom = New FontAwesome.Sharp.IconButton()
        Me.btnRepVtas = New FontAwesome.Sharp.IconButton()
        Me.btnReportes = New FontAwesome.Sharp.IconButton()
        Me.pnlNomina = New System.Windows.Forms.Panel()
        Me.btnNomRet = New FontAwesome.Sharp.IconButton()
        Me.btnNomOpr = New FontAwesome.Sharp.IconButton()
        Me.btnNomReg = New FontAwesome.Sharp.IconButton()
        Me.btnNomina = New FontAwesome.Sharp.IconButton()
        Me.pnladministracion = New System.Windows.Forms.Panel()
        Me.btnImpAdmin = New FontAwesome.Sharp.IconButton()
        Me.btnOprAdmin = New FontAwesome.Sharp.IconButton()
        Me.btnRegAdmin = New FontAwesome.Sharp.IconButton()
        Me.BtnAdministracion = New FontAwesome.Sharp.IconButton()
        Me.PnlInventario = New System.Windows.Forms.Panel()
        Me.btnOperaciones = New FontAwesome.Sharp.IconButton()
        Me.btnRegistros = New FontAwesome.Sharp.IconButton()
        Me.btnInventario = New FontAwesome.Sharp.IconButton()
        Me.pnlCompras = New System.Windows.Forms.Panel()
        Me.btnCxP = New FontAwesome.Sharp.IconButton()
        Me.btnOprCompras = New FontAwesome.Sharp.IconButton()
        Me.btnRegCompras = New FontAwesome.Sharp.IconButton()
        Me.btnCompras = New FontAwesome.Sharp.IconButton()
        Me.pnlVentas = New System.Windows.Forms.Panel()
        Me.btnvtasCxC = New FontAwesome.Sharp.IconButton()
        Me.btnvtasTra = New FontAwesome.Sharp.IconButton()
        Me.btnvtasCon = New FontAwesome.Sharp.IconButton()
        Me.btnvtasVen = New FontAwesome.Sharp.IconButton()
        Me.btnvtasDeb = New FontAwesome.Sharp.IconButton()
        Me.btnvtasCre = New FontAwesome.Sharp.IconButton()
        Me.btnvtasOpr = New FontAwesome.Sharp.IconButton()
        Me.btnvtasReg = New FontAwesome.Sharp.IconButton()
        Me.btnVentas = New FontAwesome.Sharp.IconButton()
        Me.pnlEscritorio = New System.Windows.Forms.Panel()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.pnlBarra.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.pnlMenu.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlSistemas.SuspendLayout()
        Me.pnlReportes.SuspendLayout()
        Me.pnlNomina.SuspendLayout()
        Me.pnladministracion.SuspendLayout()
        Me.PnlInventario.SuspendLayout()
        Me.pnlCompras.SuspendLayout()
        Me.pnlVentas.SuspendLayout()
        Me.pnlEscritorio.SuspendLayout()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'pnlBarra
        '
        Me.pnlBarra.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.pnlBarra.Controls.Add(Me.btnPerfil)
        Me.pnlBarra.Controls.Add(Me.btnMaximizar)
        Me.pnlBarra.Controls.Add(Me.btnRestaurar)
        Me.pnlBarra.Controls.Add(Me.btnMinimizar)
        Me.pnlBarra.Controls.Add(Me.btnCerrar)
        Me.pnlBarra.Controls.Add(Me.PictureBox1)
        Me.pnlBarra.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlBarra.Location = New System.Drawing.Point(0, 0)
        Me.pnlBarra.Name = "pnlBarra"
        Me.pnlBarra.Size = New System.Drawing.Size(1002, 70)
        Me.pnlBarra.TabIndex = 1
        '
        'btnPerfil
        '
        Me.btnPerfil.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnPerfil.FlatAppearance.BorderSize = 0
        Me.btnPerfil.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnPerfil.Font = New System.Drawing.Font("Arial Narrow", 6.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnPerfil.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnPerfil.IconChar = FontAwesome.Sharp.IconChar.AngleDown
        Me.btnPerfil.IconColor = System.Drawing.Color.Gainsboro
        Me.btnPerfil.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnPerfil.IconSize = 20
        Me.btnPerfil.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnPerfil.Location = New System.Drawing.Point(863, 39)
        Me.btnPerfil.Name = "btnPerfil"
        Me.btnPerfil.Size = New System.Drawing.Size(100, 25)
        Me.btnPerfil.TabIndex = 27
        Me.btnPerfil.Text = "USUARIO"
        Me.btnPerfil.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnPerfil.UseVisualStyleBackColor = True
        '
        'btnMaximizar
        '
        Me.btnMaximizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMaximizar.FlatAppearance.BorderSize = 0
        Me.btnMaximizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue
        Me.btnMaximizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMaximizar.IconChar = FontAwesome.Sharp.IconChar.WindowMaximize
        Me.btnMaximizar.IconColor = System.Drawing.Color.White
        Me.btnMaximizar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnMaximizar.IconSize = 20
        Me.btnMaximizar.Location = New System.Drawing.Point(953, 3)
        Me.btnMaximizar.Name = "btnMaximizar"
        Me.btnMaximizar.Size = New System.Drawing.Size(20, 20)
        Me.btnMaximizar.TabIndex = 23
        Me.btnMaximizar.UseVisualStyleBackColor = True
        '
        'btnRestaurar
        '
        Me.btnRestaurar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnRestaurar.FlatAppearance.BorderSize = 0
        Me.btnRestaurar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue
        Me.btnRestaurar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRestaurar.IconChar = FontAwesome.Sharp.IconChar.WindowRestore
        Me.btnRestaurar.IconColor = System.Drawing.Color.White
        Me.btnRestaurar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRestaurar.IconSize = 20
        Me.btnRestaurar.Location = New System.Drawing.Point(953, 3)
        Me.btnRestaurar.Name = "btnRestaurar"
        Me.btnRestaurar.Padding = New System.Windows.Forms.Padding(0, 0, 10, 0)
        Me.btnRestaurar.Size = New System.Drawing.Size(20, 20)
        Me.btnRestaurar.TabIndex = 25
        Me.btnRestaurar.UseVisualStyleBackColor = True
        '
        'btnMinimizar
        '
        Me.btnMinimizar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnMinimizar.FlatAppearance.BorderSize = 0
        Me.btnMinimizar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue
        Me.btnMinimizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnMinimizar.IconChar = FontAwesome.Sharp.IconChar.WindowMinimize
        Me.btnMinimizar.IconColor = System.Drawing.Color.White
        Me.btnMinimizar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnMinimizar.IconSize = 20
        Me.btnMinimizar.Location = New System.Drawing.Point(925, 3)
        Me.btnMinimizar.Name = "btnMinimizar"
        Me.btnMinimizar.Size = New System.Drawing.Size(20, 20)
        Me.btnMinimizar.TabIndex = 24
        Me.btnMinimizar.UseVisualStyleBackColor = True
        '
        'btnCerrar
        '
        Me.btnCerrar.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.btnCerrar.FlatAppearance.BorderSize = 0
        Me.btnCerrar.FlatAppearance.MouseOverBackColor = System.Drawing.Color.CornflowerBlue
        Me.btnCerrar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCerrar.IconChar = FontAwesome.Sharp.IconChar.WindowClose
        Me.btnCerrar.IconColor = System.Drawing.Color.White
        Me.btnCerrar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnCerrar.IconSize = 20
        Me.btnCerrar.Location = New System.Drawing.Point(979, 3)
        Me.btnCerrar.Name = "btnCerrar"
        Me.btnCerrar.Size = New System.Drawing.Size(20, 20)
        Me.btnCerrar.TabIndex = 22
        Me.btnCerrar.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(0, 0)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(220, 70)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'pnlStatus
        '
        Me.pnlStatus.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.pnlStatus.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.pnlStatus.Location = New System.Drawing.Point(0, 620)
        Me.pnlStatus.Name = "pnlStatus"
        Me.pnlStatus.Size = New System.Drawing.Size(1002, 30)
        Me.pnlStatus.TabIndex = 4
        '
        'pnlMenu
        '
        Me.pnlMenu.AutoScroll = True
        Me.pnlMenu.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.pnlMenu.Controls.Add(Me.Panel1)
        Me.pnlMenu.Controls.Add(Me.pnlSistemas)
        Me.pnlMenu.Controls.Add(Me.btnSistema)
        Me.pnlMenu.Controls.Add(Me.pnlReportes)
        Me.pnlMenu.Controls.Add(Me.btnReportes)
        Me.pnlMenu.Controls.Add(Me.pnlNomina)
        Me.pnlMenu.Controls.Add(Me.btnNomina)
        Me.pnlMenu.Controls.Add(Me.pnladministracion)
        Me.pnlMenu.Controls.Add(Me.BtnAdministracion)
        Me.pnlMenu.Controls.Add(Me.PnlInventario)
        Me.pnlMenu.Controls.Add(Me.btnInventario)
        Me.pnlMenu.Controls.Add(Me.pnlCompras)
        Me.pnlMenu.Controls.Add(Me.btnCompras)
        Me.pnlMenu.Controls.Add(Me.pnlVentas)
        Me.pnlMenu.Controls.Add(Me.btnVentas)
        Me.pnlMenu.Dock = System.Windows.Forms.DockStyle.Left
        Me.pnlMenu.Location = New System.Drawing.Point(0, 70)
        Me.pnlMenu.Name = "pnlMenu"
        Me.pnlMenu.Size = New System.Drawing.Size(220, 550)
        Me.pnlMenu.TabIndex = 5
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.btnHome)
        Me.Panel1.Controls.Add(Me.btnSalir)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 1868)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(203, 40)
        Me.Panel1.TabIndex = 40
        '
        'btnHome
        '
        Me.btnHome.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnHome.Dock = System.Windows.Forms.DockStyle.Right
        Me.btnHome.FlatAppearance.BorderSize = 0
        Me.btnHome.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnHome.ForeColor = System.Drawing.Color.Transparent
        Me.btnHome.IconChar = FontAwesome.Sharp.IconChar.Home
        Me.btnHome.IconColor = System.Drawing.Color.White
        Me.btnHome.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnHome.IconSize = 32
        Me.btnHome.ImageAlign = System.Drawing.ContentAlignment.BottomRight
        Me.btnHome.Location = New System.Drawing.Point(173, 0)
        Me.btnHome.Name = "btnHome"
        Me.btnHome.Size = New System.Drawing.Size(30, 40)
        Me.btnHome.TabIndex = 41
        Me.btnHome.Tag = "DashBoard"
        Me.btnHome.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnHome.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnHome.UseVisualStyleBackColor = True
        '
        'btnSalir
        '
        Me.btnSalir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSalir.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnSalir.FlatAppearance.BorderSize = 0
        Me.btnSalir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSalir.ForeColor = System.Drawing.Color.White
        Me.btnSalir.IconChar = FontAwesome.Sharp.IconChar.PowerOff
        Me.btnSalir.IconColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.btnSalir.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSalir.IconSize = 32
        Me.btnSalir.ImageAlign = System.Drawing.ContentAlignment.BottomRight
        Me.btnSalir.Location = New System.Drawing.Point(0, 0)
        Me.btnSalir.Name = "btnSalir"
        Me.btnSalir.Size = New System.Drawing.Size(30, 40)
        Me.btnSalir.TabIndex = 40
        Me.btnSalir.Tag = "DashBoard"
        Me.btnSalir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSalir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSalir.UseVisualStyleBackColor = True
        '
        'pnlSistemas
        '
        Me.pnlSistemas.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.pnlSistemas.Controls.Add(Me.btnSisResp)
        Me.pnlSistemas.Controls.Add(Me.btnSisEjCon)
        Me.pnlSistemas.Controls.Add(Me.btnSisEjCo)
        Me.pnlSistemas.Controls.Add(Me.btnSisEjAc)
        Me.pnlSistemas.Controls.Add(Me.btnSisImpF)
        Me.pnlSistemas.Controls.Add(Me.btnSisPara)
        Me.pnlSistemas.Controls.Add(Me.btnSisOpcS)
        Me.pnlSistemas.Controls.Add(Me.btnSisPfac)
        Me.pnlSistemas.Controls.Add(Me.btnSisCEmp)
        Me.pnlSistemas.Controls.Add(Me.btnSisEmp)
        Me.pnlSistemas.Controls.Add(Me.btnSisCla)
        Me.pnlSistemas.Controls.Add(Me.btnSisUsr)
        Me.pnlSistemas.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlSistemas.Location = New System.Drawing.Point(0, 1379)
        Me.pnlSistemas.Name = "pnlSistemas"
        Me.pnlSistemas.Size = New System.Drawing.Size(203, 489)
        Me.pnlSistemas.TabIndex = 47
        '
        'btnSisResp
        '
        Me.btnSisResp.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisResp.FlatAppearance.BorderSize = 0
        Me.btnSisResp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisResp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisResp.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisResp.IconChar = FontAwesome.Sharp.IconChar.Database
        Me.btnSisResp.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisResp.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisResp.IconSize = 32
        Me.btnSisResp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisResp.Location = New System.Drawing.Point(0, 440)
        Me.btnSisResp.Name = "btnSisResp"
        Me.btnSisResp.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnSisResp.Size = New System.Drawing.Size(203, 40)
        Me.btnSisResp.TabIndex = 48
        Me.btnSisResp.Text = "      Respaldos"
        Me.btnSisResp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisResp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisResp.UseVisualStyleBackColor = True
        '
        'btnSisEjCon
        '
        Me.btnSisEjCon.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisEjCon.FlatAppearance.BorderSize = 0
        Me.btnSisEjCon.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisEjCon.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisEjCon.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisEjCon.IconChar = FontAwesome.Sharp.IconChar.CalendarAlt
        Me.btnSisEjCon.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisEjCon.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisEjCon.IconSize = 32
        Me.btnSisEjCon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEjCon.Location = New System.Drawing.Point(0, 400)
        Me.btnSisEjCon.Name = "btnSisEjCon"
        Me.btnSisEjCon.Padding = New System.Windows.Forms.Padding(35, 0, 10, 0)
        Me.btnSisEjCon.Size = New System.Drawing.Size(203, 40)
        Me.btnSisEjCon.TabIndex = 47
        Me.btnSisEjCon.Text = "      Ejec. Consultas"
        Me.btnSisEjCon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEjCon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisEjCon.UseVisualStyleBackColor = True
        '
        'btnSisEjCo
        '
        Me.btnSisEjCo.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisEjCo.FlatAppearance.BorderSize = 0
        Me.btnSisEjCo.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisEjCo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisEjCo.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisEjCo.IconChar = FontAwesome.Sharp.IconChar.Bolt
        Me.btnSisEjCo.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisEjCo.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisEjCo.IconSize = 32
        Me.btnSisEjCo.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEjCo.Location = New System.Drawing.Point(0, 360)
        Me.btnSisEjCo.Name = "btnSisEjCo"
        Me.btnSisEjCo.Padding = New System.Windows.Forms.Padding(35, 0, 10, 0)
        Me.btnSisEjCo.Size = New System.Drawing.Size(203, 40)
        Me.btnSisEjCo.TabIndex = 46
        Me.btnSisEjCo.Text = "      Ejec. Comandos"
        Me.btnSisEjCo.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEjCo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisEjCo.UseVisualStyleBackColor = True
        '
        'btnSisEjAc
        '
        Me.btnSisEjAc.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisEjAc.FlatAppearance.BorderSize = 0
        Me.btnSisEjAc.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisEjAc.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisEjAc.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisEjAc.IconChar = FontAwesome.Sharp.IconChar.UserCheck
        Me.btnSisEjAc.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisEjAc.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisEjAc.IconSize = 32
        Me.btnSisEjAc.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEjAc.Location = New System.Drawing.Point(0, 320)
        Me.btnSisEjAc.Name = "btnSisEjAc"
        Me.btnSisEjAc.Padding = New System.Windows.Forms.Padding(35, 0, 5, 0)
        Me.btnSisEjAc.Size = New System.Drawing.Size(203, 40)
        Me.btnSisEjAc.TabIndex = 45
        Me.btnSisEjAc.Text = "      Ejec. Acciones"
        Me.btnSisEjAc.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEjAc.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisEjAc.UseVisualStyleBackColor = True
        '
        'btnSisImpF
        '
        Me.btnSisImpF.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisImpF.FlatAppearance.BorderSize = 0
        Me.btnSisImpF.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisImpF.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisImpF.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisImpF.IconChar = FontAwesome.Sharp.IconChar.Print
        Me.btnSisImpF.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisImpF.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisImpF.IconSize = 32
        Me.btnSisImpF.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisImpF.Location = New System.Drawing.Point(0, 280)
        Me.btnSisImpF.Name = "btnSisImpF"
        Me.btnSisImpF.Padding = New System.Windows.Forms.Padding(35, 0, 10, 0)
        Me.btnSisImpF.Size = New System.Drawing.Size(203, 40)
        Me.btnSisImpF.TabIndex = 44
        Me.btnSisImpF.Text = "      Impresora Fiscal"
        Me.btnSisImpF.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisImpF.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisImpF.UseVisualStyleBackColor = True
        '
        'btnSisPara
        '
        Me.btnSisPara.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisPara.FlatAppearance.BorderSize = 0
        Me.btnSisPara.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisPara.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisPara.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisPara.IconChar = FontAwesome.Sharp.IconChar.Cogs
        Me.btnSisPara.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisPara.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisPara.IconSize = 32
        Me.btnSisPara.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisPara.Location = New System.Drawing.Point(0, 240)
        Me.btnSisPara.Name = "btnSisPara"
        Me.btnSisPara.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnSisPara.Size = New System.Drawing.Size(203, 40)
        Me.btnSisPara.TabIndex = 43
        Me.btnSisPara.Text = "      Párametros"
        Me.btnSisPara.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisPara.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisPara.UseVisualStyleBackColor = True
        '
        'btnSisOpcS
        '
        Me.btnSisOpcS.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisOpcS.FlatAppearance.BorderSize = 0
        Me.btnSisOpcS.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisOpcS.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisOpcS.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisOpcS.IconChar = FontAwesome.Sharp.IconChar.Tasks
        Me.btnSisOpcS.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisOpcS.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisOpcS.IconSize = 32
        Me.btnSisOpcS.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisOpcS.Location = New System.Drawing.Point(0, 200)
        Me.btnSisOpcS.Name = "btnSisOpcS"
        Me.btnSisOpcS.Padding = New System.Windows.Forms.Padding(35, 0, 10, 0)
        Me.btnSisOpcS.Size = New System.Drawing.Size(203, 40)
        Me.btnSisOpcS.TabIndex = 42
        Me.btnSisOpcS.Text = "      Opc. del Sistema"
        Me.btnSisOpcS.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisOpcS.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisOpcS.UseVisualStyleBackColor = True
        '
        'btnSisPfac
        '
        Me.btnSisPfac.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisPfac.FlatAppearance.BorderSize = 0
        Me.btnSisPfac.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisPfac.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisPfac.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisPfac.IconChar = FontAwesome.Sharp.IconChar.CommentDollar
        Me.btnSisPfac.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisPfac.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisPfac.IconSize = 32
        Me.btnSisPfac.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisPfac.Location = New System.Drawing.Point(0, 160)
        Me.btnSisPfac.Name = "btnSisPfac"
        Me.btnSisPfac.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnSisPfac.Size = New System.Drawing.Size(203, 40)
        Me.btnSisPfac.TabIndex = 41
        Me.btnSisPfac.Text = "      Paga Facil"
        Me.btnSisPfac.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisPfac.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisPfac.UseVisualStyleBackColor = True
        '
        'btnSisCEmp
        '
        Me.btnSisCEmp.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisCEmp.FlatAppearance.BorderSize = 0
        Me.btnSisCEmp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisCEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisCEmp.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisCEmp.IconChar = FontAwesome.Sharp.IconChar.MapSigns
        Me.btnSisCEmp.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisCEmp.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisCEmp.IconSize = 32
        Me.btnSisCEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisCEmp.Location = New System.Drawing.Point(0, 120)
        Me.btnSisCEmp.Name = "btnSisCEmp"
        Me.btnSisCEmp.Padding = New System.Windows.Forms.Padding(35, 0, 10, 0)
        Me.btnSisCEmp.Size = New System.Drawing.Size(203, 40)
        Me.btnSisCEmp.TabIndex = 40
        Me.btnSisCEmp.Text = "      Cambio de Emp."
        Me.btnSisCEmp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisCEmp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisCEmp.UseVisualStyleBackColor = True
        '
        'btnSisEmp
        '
        Me.btnSisEmp.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisEmp.FlatAppearance.BorderSize = 0
        Me.btnSisEmp.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisEmp.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisEmp.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisEmp.IconChar = FontAwesome.Sharp.IconChar.Building
        Me.btnSisEmp.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisEmp.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisEmp.IconSize = 32
        Me.btnSisEmp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEmp.Location = New System.Drawing.Point(0, 80)
        Me.btnSisEmp.Name = "btnSisEmp"
        Me.btnSisEmp.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnSisEmp.Size = New System.Drawing.Size(203, 40)
        Me.btnSisEmp.TabIndex = 39
        Me.btnSisEmp.Text = "      Empresas"
        Me.btnSisEmp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisEmp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisEmp.UseVisualStyleBackColor = True
        '
        'btnSisCla
        '
        Me.btnSisCla.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisCla.FlatAppearance.BorderSize = 0
        Me.btnSisCla.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisCla.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisCla.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisCla.IconChar = FontAwesome.Sharp.IconChar.ICursor
        Me.btnSisCla.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisCla.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisCla.IconSize = 32
        Me.btnSisCla.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisCla.Location = New System.Drawing.Point(0, 40)
        Me.btnSisCla.Name = "btnSisCla"
        Me.btnSisCla.Padding = New System.Windows.Forms.Padding(35, 0, 10, 0)
        Me.btnSisCla.Size = New System.Drawing.Size(203, 40)
        Me.btnSisCla.TabIndex = 38
        Me.btnSisCla.Text = "      Cambio de Clave"
        Me.btnSisCla.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisCla.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisCla.UseVisualStyleBackColor = True
        '
        'btnSisUsr
        '
        Me.btnSisUsr.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSisUsr.FlatAppearance.BorderSize = 0
        Me.btnSisUsr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSisUsr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSisUsr.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSisUsr.IconChar = FontAwesome.Sharp.IconChar.AddressCard
        Me.btnSisUsr.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSisUsr.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSisUsr.IconSize = 32
        Me.btnSisUsr.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisUsr.Location = New System.Drawing.Point(0, 0)
        Me.btnSisUsr.Name = "btnSisUsr"
        Me.btnSisUsr.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnSisUsr.Size = New System.Drawing.Size(203, 40)
        Me.btnSisUsr.TabIndex = 37
        Me.btnSisUsr.Text = "      Usuarios"
        Me.btnSisUsr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSisUsr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSisUsr.UseVisualStyleBackColor = True
        '
        'btnSistema
        '
        Me.btnSistema.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnSistema.FlatAppearance.BorderSize = 0
        Me.btnSistema.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSistema.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnSistema.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnSistema.IconChar = FontAwesome.Sharp.IconChar.Tools
        Me.btnSistema.IconColor = System.Drawing.Color.Gainsboro
        Me.btnSistema.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSistema.IconSize = 32
        Me.btnSistema.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSistema.Location = New System.Drawing.Point(0, 1319)
        Me.btnSistema.Name = "btnSistema"
        Me.btnSistema.Padding = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.btnSistema.Size = New System.Drawing.Size(203, 60)
        Me.btnSistema.TabIndex = 36
        Me.btnSistema.Text = "      Sistema"
        Me.btnSistema.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnSistema.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnSistema.UseVisualStyleBackColor = True
        '
        'pnlReportes
        '
        Me.pnlReportes.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.pnlReportes.Controls.Add(Me.btnRepAdm)
        Me.pnlReportes.Controls.Add(Me.btnRepInv)
        Me.pnlReportes.Controls.Add(Me.btnRepCom)
        Me.pnlReportes.Controls.Add(Me.btnRepVtas)
        Me.pnlReportes.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlReportes.Location = New System.Drawing.Point(0, 1154)
        Me.pnlReportes.Name = "pnlReportes"
        Me.pnlReportes.Size = New System.Drawing.Size(203, 165)
        Me.pnlReportes.TabIndex = 46
        '
        'btnRepAdm
        '
        Me.btnRepAdm.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnRepAdm.FlatAppearance.BorderSize = 0
        Me.btnRepAdm.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRepAdm.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRepAdm.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnRepAdm.IconChar = FontAwesome.Sharp.IconChar.FileInvoice
        Me.btnRepAdm.IconColor = System.Drawing.Color.Gainsboro
        Me.btnRepAdm.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRepAdm.IconSize = 32
        Me.btnRepAdm.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepAdm.Location = New System.Drawing.Point(0, 120)
        Me.btnRepAdm.Name = "btnRepAdm"
        Me.btnRepAdm.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnRepAdm.Size = New System.Drawing.Size(203, 40)
        Me.btnRepAdm.TabIndex = 40
        Me.btnRepAdm.Text = "      Administración"
        Me.btnRepAdm.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepAdm.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRepAdm.UseVisualStyleBackColor = True
        '
        'btnRepInv
        '
        Me.btnRepInv.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnRepInv.FlatAppearance.BorderSize = 0
        Me.btnRepInv.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRepInv.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRepInv.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnRepInv.IconChar = FontAwesome.Sharp.IconChar.Archive
        Me.btnRepInv.IconColor = System.Drawing.Color.Gainsboro
        Me.btnRepInv.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRepInv.IconSize = 32
        Me.btnRepInv.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepInv.Location = New System.Drawing.Point(0, 80)
        Me.btnRepInv.Name = "btnRepInv"
        Me.btnRepInv.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnRepInv.Size = New System.Drawing.Size(203, 40)
        Me.btnRepInv.TabIndex = 39
        Me.btnRepInv.Text = "      Inventario"
        Me.btnRepInv.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepInv.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRepInv.UseVisualStyleBackColor = True
        '
        'btnRepCom
        '
        Me.btnRepCom.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnRepCom.FlatAppearance.BorderSize = 0
        Me.btnRepCom.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRepCom.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRepCom.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnRepCom.IconChar = FontAwesome.Sharp.IconChar.FileInvoiceDollar
        Me.btnRepCom.IconColor = System.Drawing.Color.Gainsboro
        Me.btnRepCom.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRepCom.IconSize = 32
        Me.btnRepCom.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepCom.Location = New System.Drawing.Point(0, 40)
        Me.btnRepCom.Name = "btnRepCom"
        Me.btnRepCom.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnRepCom.Size = New System.Drawing.Size(203, 40)
        Me.btnRepCom.TabIndex = 38
        Me.btnRepCom.Text = "      Compras"
        Me.btnRepCom.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepCom.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRepCom.UseVisualStyleBackColor = True
        '
        'btnRepVtas
        '
        Me.btnRepVtas.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnRepVtas.FlatAppearance.BorderSize = 0
        Me.btnRepVtas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRepVtas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRepVtas.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnRepVtas.IconChar = FontAwesome.Sharp.IconChar.ChartArea
        Me.btnRepVtas.IconColor = System.Drawing.Color.Gainsboro
        Me.btnRepVtas.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRepVtas.IconSize = 32
        Me.btnRepVtas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepVtas.Location = New System.Drawing.Point(0, 0)
        Me.btnRepVtas.Name = "btnRepVtas"
        Me.btnRepVtas.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnRepVtas.Size = New System.Drawing.Size(203, 40)
        Me.btnRepVtas.TabIndex = 37
        Me.btnRepVtas.Text = "      Ventas"
        Me.btnRepVtas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRepVtas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRepVtas.UseVisualStyleBackColor = True
        '
        'btnReportes
        '
        Me.btnReportes.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnReportes.FlatAppearance.BorderSize = 0
        Me.btnReportes.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnReportes.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnReportes.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnReportes.IconChar = FontAwesome.Sharp.IconChar.FileInvoice
        Me.btnReportes.IconColor = System.Drawing.Color.Gainsboro
        Me.btnReportes.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnReportes.IconSize = 32
        Me.btnReportes.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReportes.Location = New System.Drawing.Point(0, 1094)
        Me.btnReportes.Name = "btnReportes"
        Me.btnReportes.Padding = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.btnReportes.Size = New System.Drawing.Size(203, 60)
        Me.btnReportes.TabIndex = 35
        Me.btnReportes.Text = "      Reportes"
        Me.btnReportes.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnReportes.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnReportes.UseVisualStyleBackColor = True
        '
        'pnlNomina
        '
        Me.pnlNomina.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.pnlNomina.Controls.Add(Me.btnNomRet)
        Me.pnlNomina.Controls.Add(Me.btnNomOpr)
        Me.pnlNomina.Controls.Add(Me.btnNomReg)
        Me.pnlNomina.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlNomina.Location = New System.Drawing.Point(0, 967)
        Me.pnlNomina.Name = "pnlNomina"
        Me.pnlNomina.Size = New System.Drawing.Size(203, 127)
        Me.pnlNomina.TabIndex = 45
        '
        'btnNomRet
        '
        Me.btnNomRet.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnNomRet.FlatAppearance.BorderSize = 0
        Me.btnNomRet.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNomRet.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNomRet.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnNomRet.IconChar = FontAwesome.Sharp.IconChar.BalanceScale
        Me.btnNomRet.IconColor = System.Drawing.Color.Gainsboro
        Me.btnNomRet.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnNomRet.IconSize = 32
        Me.btnNomRet.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomRet.Location = New System.Drawing.Point(0, 80)
        Me.btnNomRet.Name = "btnNomRet"
        Me.btnNomRet.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnNomRet.Size = New System.Drawing.Size(203, 40)
        Me.btnNomRet.TabIndex = 39
        Me.btnNomRet.Text = "      Retenciones"
        Me.btnNomRet.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomRet.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNomRet.UseVisualStyleBackColor = True
        '
        'btnNomOpr
        '
        Me.btnNomOpr.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnNomOpr.FlatAppearance.BorderSize = 0
        Me.btnNomOpr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNomOpr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNomOpr.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnNomOpr.IconChar = FontAwesome.Sharp.IconChar.Calculator
        Me.btnNomOpr.IconColor = System.Drawing.Color.Gainsboro
        Me.btnNomOpr.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnNomOpr.IconSize = 32
        Me.btnNomOpr.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomOpr.Location = New System.Drawing.Point(0, 40)
        Me.btnNomOpr.Name = "btnNomOpr"
        Me.btnNomOpr.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnNomOpr.Size = New System.Drawing.Size(203, 40)
        Me.btnNomOpr.TabIndex = 38
        Me.btnNomOpr.Text = "      Operaciones"
        Me.btnNomOpr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomOpr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNomOpr.UseVisualStyleBackColor = True
        '
        'btnNomReg
        '
        Me.btnNomReg.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnNomReg.FlatAppearance.BorderSize = 0
        Me.btnNomReg.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNomReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNomReg.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnNomReg.IconChar = FontAwesome.Sharp.IconChar.Edit
        Me.btnNomReg.IconColor = System.Drawing.Color.Gainsboro
        Me.btnNomReg.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnNomReg.IconSize = 32
        Me.btnNomReg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomReg.Location = New System.Drawing.Point(0, 0)
        Me.btnNomReg.Name = "btnNomReg"
        Me.btnNomReg.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnNomReg.Size = New System.Drawing.Size(203, 40)
        Me.btnNomReg.TabIndex = 37
        Me.btnNomReg.Text = "      Registros"
        Me.btnNomReg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomReg.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNomReg.UseVisualStyleBackColor = True
        '
        'btnNomina
        '
        Me.btnNomina.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnNomina.FlatAppearance.BorderSize = 0
        Me.btnNomina.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnNomina.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnNomina.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnNomina.IconChar = FontAwesome.Sharp.IconChar.Users
        Me.btnNomina.IconColor = System.Drawing.Color.Gainsboro
        Me.btnNomina.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnNomina.IconSize = 32
        Me.btnNomina.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomina.Location = New System.Drawing.Point(0, 907)
        Me.btnNomina.Name = "btnNomina"
        Me.btnNomina.Padding = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.btnNomina.Size = New System.Drawing.Size(203, 60)
        Me.btnNomina.TabIndex = 34
        Me.btnNomina.Text = "      Nómina"
        Me.btnNomina.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnNomina.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnNomina.UseVisualStyleBackColor = True
        '
        'pnladministracion
        '
        Me.pnladministracion.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.pnladministracion.Controls.Add(Me.btnImpAdmin)
        Me.pnladministracion.Controls.Add(Me.btnOprAdmin)
        Me.pnladministracion.Controls.Add(Me.btnRegAdmin)
        Me.pnladministracion.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnladministracion.Location = New System.Drawing.Point(0, 780)
        Me.pnladministracion.Name = "pnladministracion"
        Me.pnladministracion.Size = New System.Drawing.Size(203, 127)
        Me.pnladministracion.TabIndex = 43
        '
        'btnImpAdmin
        '
        Me.btnImpAdmin.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnImpAdmin.FlatAppearance.BorderSize = 0
        Me.btnImpAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImpAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnImpAdmin.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnImpAdmin.IconChar = FontAwesome.Sharp.IconChar.HandHoldingUsd
        Me.btnImpAdmin.IconColor = System.Drawing.Color.Gainsboro
        Me.btnImpAdmin.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnImpAdmin.IconSize = 32
        Me.btnImpAdmin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImpAdmin.Location = New System.Drawing.Point(0, 80)
        Me.btnImpAdmin.Name = "btnImpAdmin"
        Me.btnImpAdmin.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnImpAdmin.Size = New System.Drawing.Size(203, 40)
        Me.btnImpAdmin.TabIndex = 39
        Me.btnImpAdmin.Text = "      Impuestos"
        Me.btnImpAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnImpAdmin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnImpAdmin.UseVisualStyleBackColor = True
        '
        'btnOprAdmin
        '
        Me.btnOprAdmin.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnOprAdmin.FlatAppearance.BorderSize = 0
        Me.btnOprAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOprAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOprAdmin.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnOprAdmin.IconChar = FontAwesome.Sharp.IconChar.Landmark
        Me.btnOprAdmin.IconColor = System.Drawing.Color.Gainsboro
        Me.btnOprAdmin.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnOprAdmin.IconSize = 32
        Me.btnOprAdmin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOprAdmin.Location = New System.Drawing.Point(0, 40)
        Me.btnOprAdmin.Name = "btnOprAdmin"
        Me.btnOprAdmin.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnOprAdmin.Size = New System.Drawing.Size(203, 40)
        Me.btnOprAdmin.TabIndex = 38
        Me.btnOprAdmin.Text = "      Operaciones"
        Me.btnOprAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOprAdmin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOprAdmin.UseVisualStyleBackColor = True
        '
        'btnRegAdmin
        '
        Me.btnRegAdmin.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnRegAdmin.FlatAppearance.BorderSize = 0
        Me.btnRegAdmin.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRegAdmin.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegAdmin.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnRegAdmin.IconChar = FontAwesome.Sharp.IconChar.Edit
        Me.btnRegAdmin.IconColor = System.Drawing.Color.Gainsboro
        Me.btnRegAdmin.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRegAdmin.IconSize = 32
        Me.btnRegAdmin.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegAdmin.Location = New System.Drawing.Point(0, 0)
        Me.btnRegAdmin.Name = "btnRegAdmin"
        Me.btnRegAdmin.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnRegAdmin.Size = New System.Drawing.Size(203, 40)
        Me.btnRegAdmin.TabIndex = 37
        Me.btnRegAdmin.Text = "      Registros"
        Me.btnRegAdmin.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegAdmin.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRegAdmin.UseVisualStyleBackColor = True
        '
        'BtnAdministracion
        '
        Me.BtnAdministracion.Dock = System.Windows.Forms.DockStyle.Top
        Me.BtnAdministracion.FlatAppearance.BorderSize = 0
        Me.BtnAdministracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.BtnAdministracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.BtnAdministracion.ForeColor = System.Drawing.Color.Gainsboro
        Me.BtnAdministracion.IconChar = FontAwesome.Sharp.IconChar.Sitemap
        Me.BtnAdministracion.IconColor = System.Drawing.Color.Gainsboro
        Me.BtnAdministracion.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.BtnAdministracion.IconSize = 32
        Me.BtnAdministracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnAdministracion.Location = New System.Drawing.Point(0, 720)
        Me.BtnAdministracion.Name = "BtnAdministracion"
        Me.BtnAdministracion.Padding = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.BtnAdministracion.Size = New System.Drawing.Size(203, 60)
        Me.BtnAdministracion.TabIndex = 33
        Me.BtnAdministracion.Text = "      Administración"
        Me.BtnAdministracion.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.BtnAdministracion.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.BtnAdministracion.UseVisualStyleBackColor = True
        '
        'PnlInventario
        '
        Me.PnlInventario.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.PnlInventario.Controls.Add(Me.btnOperaciones)
        Me.PnlInventario.Controls.Add(Me.btnRegistros)
        Me.PnlInventario.Dock = System.Windows.Forms.DockStyle.Top
        Me.PnlInventario.Location = New System.Drawing.Point(0, 634)
        Me.PnlInventario.Name = "PnlInventario"
        Me.PnlInventario.Size = New System.Drawing.Size(203, 86)
        Me.PnlInventario.TabIndex = 41
        '
        'btnOperaciones
        '
        Me.btnOperaciones.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnOperaciones.FlatAppearance.BorderSize = 0
        Me.btnOperaciones.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOperaciones.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOperaciones.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnOperaciones.IconChar = FontAwesome.Sharp.IconChar.PeopleCarry
        Me.btnOperaciones.IconColor = System.Drawing.Color.Gainsboro
        Me.btnOperaciones.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnOperaciones.IconSize = 32
        Me.btnOperaciones.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOperaciones.Location = New System.Drawing.Point(0, 40)
        Me.btnOperaciones.Name = "btnOperaciones"
        Me.btnOperaciones.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnOperaciones.Size = New System.Drawing.Size(203, 40)
        Me.btnOperaciones.TabIndex = 38
        Me.btnOperaciones.Text = "      Operaciones"
        Me.btnOperaciones.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOperaciones.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOperaciones.UseVisualStyleBackColor = True
        '
        'btnRegistros
        '
        Me.btnRegistros.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnRegistros.FlatAppearance.BorderSize = 0
        Me.btnRegistros.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRegistros.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegistros.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnRegistros.IconChar = FontAwesome.Sharp.IconChar.Edit
        Me.btnRegistros.IconColor = System.Drawing.Color.Gainsboro
        Me.btnRegistros.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRegistros.IconSize = 32
        Me.btnRegistros.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistros.Location = New System.Drawing.Point(0, 0)
        Me.btnRegistros.Name = "btnRegistros"
        Me.btnRegistros.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnRegistros.Size = New System.Drawing.Size(203, 40)
        Me.btnRegistros.TabIndex = 37
        Me.btnRegistros.Text = "      Registros"
        Me.btnRegistros.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegistros.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRegistros.UseVisualStyleBackColor = True
        '
        'btnInventario
        '
        Me.btnInventario.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnInventario.FlatAppearance.BorderSize = 0
        Me.btnInventario.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnInventario.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnInventario.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnInventario.IconChar = FontAwesome.Sharp.IconChar.Dolly
        Me.btnInventario.IconColor = System.Drawing.Color.Gainsboro
        Me.btnInventario.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnInventario.IconSize = 32
        Me.btnInventario.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnInventario.Location = New System.Drawing.Point(0, 574)
        Me.btnInventario.Name = "btnInventario"
        Me.btnInventario.Padding = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.btnInventario.Size = New System.Drawing.Size(203, 60)
        Me.btnInventario.TabIndex = 32
        Me.btnInventario.Text = "      Inventario"
        Me.btnInventario.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnInventario.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnInventario.UseVisualStyleBackColor = True
        '
        'pnlCompras
        '
        Me.pnlCompras.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.pnlCompras.Controls.Add(Me.btnCxP)
        Me.pnlCompras.Controls.Add(Me.btnOprCompras)
        Me.pnlCompras.Controls.Add(Me.btnRegCompras)
        Me.pnlCompras.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlCompras.Location = New System.Drawing.Point(0, 446)
        Me.pnlCompras.Name = "pnlCompras"
        Me.pnlCompras.Size = New System.Drawing.Size(203, 128)
        Me.pnlCompras.TabIndex = 42
        '
        'btnCxP
        '
        Me.btnCxP.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnCxP.FlatAppearance.BorderSize = 0
        Me.btnCxP.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCxP.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCxP.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnCxP.IconChar = FontAwesome.Sharp.IconChar.FileInvoiceDollar
        Me.btnCxP.IconColor = System.Drawing.Color.Gainsboro
        Me.btnCxP.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnCxP.IconSize = 32
        Me.btnCxP.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCxP.Location = New System.Drawing.Point(0, 80)
        Me.btnCxP.Name = "btnCxP"
        Me.btnCxP.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnCxP.Size = New System.Drawing.Size(203, 40)
        Me.btnCxP.TabIndex = 39
        Me.btnCxP.Text = "      Ctas x Pagar"
        Me.btnCxP.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCxP.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCxP.UseVisualStyleBackColor = True
        '
        'btnOprCompras
        '
        Me.btnOprCompras.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnOprCompras.FlatAppearance.BorderSize = 0
        Me.btnOprCompras.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnOprCompras.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnOprCompras.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnOprCompras.IconChar = FontAwesome.Sharp.IconChar.Barcode
        Me.btnOprCompras.IconColor = System.Drawing.Color.Gainsboro
        Me.btnOprCompras.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnOprCompras.IconSize = 32
        Me.btnOprCompras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOprCompras.Location = New System.Drawing.Point(0, 40)
        Me.btnOprCompras.Name = "btnOprCompras"
        Me.btnOprCompras.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnOprCompras.Size = New System.Drawing.Size(203, 40)
        Me.btnOprCompras.TabIndex = 38
        Me.btnOprCompras.Text = "      Operaciones"
        Me.btnOprCompras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnOprCompras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnOprCompras.UseVisualStyleBackColor = True
        '
        'btnRegCompras
        '
        Me.btnRegCompras.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnRegCompras.FlatAppearance.BorderSize = 0
        Me.btnRegCompras.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRegCompras.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRegCompras.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnRegCompras.IconChar = FontAwesome.Sharp.IconChar.Edit
        Me.btnRegCompras.IconColor = System.Drawing.Color.Gainsboro
        Me.btnRegCompras.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnRegCompras.IconSize = 32
        Me.btnRegCompras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegCompras.Location = New System.Drawing.Point(0, 0)
        Me.btnRegCompras.Name = "btnRegCompras"
        Me.btnRegCompras.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnRegCompras.Size = New System.Drawing.Size(203, 40)
        Me.btnRegCompras.TabIndex = 37
        Me.btnRegCompras.Text = "      Registros"
        Me.btnRegCompras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnRegCompras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnRegCompras.UseVisualStyleBackColor = True
        '
        'btnCompras
        '
        Me.btnCompras.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnCompras.FlatAppearance.BorderSize = 0
        Me.btnCompras.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCompras.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnCompras.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnCompras.IconChar = FontAwesome.Sharp.IconChar.Truck
        Me.btnCompras.IconColor = System.Drawing.Color.Gainsboro
        Me.btnCompras.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnCompras.IconSize = 32
        Me.btnCompras.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompras.Location = New System.Drawing.Point(0, 386)
        Me.btnCompras.Name = "btnCompras"
        Me.btnCompras.Padding = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.btnCompras.Size = New System.Drawing.Size(203, 60)
        Me.btnCompras.TabIndex = 31
        Me.btnCompras.Text = "      Compras"
        Me.btnCompras.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnCompras.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnCompras.UseVisualStyleBackColor = True
        '
        'pnlVentas
        '
        Me.pnlVentas.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.pnlVentas.Controls.Add(Me.btnvtasCxC)
        Me.pnlVentas.Controls.Add(Me.btnvtasTra)
        Me.pnlVentas.Controls.Add(Me.btnvtasCon)
        Me.pnlVentas.Controls.Add(Me.btnvtasVen)
        Me.pnlVentas.Controls.Add(Me.btnvtasDeb)
        Me.pnlVentas.Controls.Add(Me.btnvtasCre)
        Me.pnlVentas.Controls.Add(Me.btnvtasOpr)
        Me.pnlVentas.Controls.Add(Me.btnvtasReg)
        Me.pnlVentas.Dock = System.Windows.Forms.DockStyle.Top
        Me.pnlVentas.Location = New System.Drawing.Point(0, 60)
        Me.pnlVentas.Name = "pnlVentas"
        Me.pnlVentas.Size = New System.Drawing.Size(203, 326)
        Me.pnlVentas.TabIndex = 44
        '
        'btnvtasCxC
        '
        Me.btnvtasCxC.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasCxC.FlatAppearance.BorderSize = 0
        Me.btnvtasCxC.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasCxC.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasCxC.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasCxC.IconChar = FontAwesome.Sharp.IconChar.CashRegister
        Me.btnvtasCxC.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasCxC.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasCxC.IconSize = 32
        Me.btnvtasCxC.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasCxC.Location = New System.Drawing.Point(0, 280)
        Me.btnvtasCxC.Name = "btnvtasCxC"
        Me.btnvtasCxC.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasCxC.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasCxC.TabIndex = 44
        Me.btnvtasCxC.Text = "      Ctas x Cobrar"
        Me.btnvtasCxC.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasCxC.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasCxC.UseVisualStyleBackColor = True
        '
        'btnvtasTra
        '
        Me.btnvtasTra.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasTra.FlatAppearance.BorderSize = 0
        Me.btnvtasTra.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasTra.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasTra.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasTra.IconChar = FontAwesome.Sharp.IconChar.ShippingFast
        Me.btnvtasTra.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasTra.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasTra.IconSize = 32
        Me.btnvtasTra.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasTra.Location = New System.Drawing.Point(0, 240)
        Me.btnvtasTra.Name = "btnvtasTra"
        Me.btnvtasTra.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasTra.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasTra.TabIndex = 43
        Me.btnvtasTra.Text = "      Transportes"
        Me.btnvtasTra.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasTra.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasTra.UseVisualStyleBackColor = True
        '
        'btnvtasCon
        '
        Me.btnvtasCon.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasCon.FlatAppearance.BorderSize = 0
        Me.btnvtasCon.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasCon.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasCon.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasCon.IconChar = FontAwesome.Sharp.IconChar.FileAlt
        Me.btnvtasCon.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasCon.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasCon.IconSize = 32
        Me.btnvtasCon.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasCon.Location = New System.Drawing.Point(0, 200)
        Me.btnvtasCon.Name = "btnvtasCon"
        Me.btnvtasCon.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasCon.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasCon.TabIndex = 42
        Me.btnvtasCon.Text = "      Contratos"
        Me.btnvtasCon.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasCon.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasCon.UseVisualStyleBackColor = True
        '
        'btnvtasVen
        '
        Me.btnvtasVen.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasVen.FlatAppearance.BorderSize = 0
        Me.btnvtasVen.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasVen.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasVen.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasVen.IconChar = FontAwesome.Sharp.IconChar.PeopleArrows
        Me.btnvtasVen.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasVen.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasVen.IconSize = 32
        Me.btnvtasVen.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasVen.Location = New System.Drawing.Point(0, 160)
        Me.btnvtasVen.Name = "btnvtasVen"
        Me.btnvtasVen.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasVen.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasVen.TabIndex = 41
        Me.btnvtasVen.Text = "      Vendedores"
        Me.btnvtasVen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasVen.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasVen.UseVisualStyleBackColor = True
        '
        'btnvtasDeb
        '
        Me.btnvtasDeb.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasDeb.FlatAppearance.BorderSize = 0
        Me.btnvtasDeb.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasDeb.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasDeb.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasDeb.IconChar = FontAwesome.Sharp.IconChar.CreditCard
        Me.btnvtasDeb.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasDeb.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasDeb.IconSize = 32
        Me.btnvtasDeb.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasDeb.Location = New System.Drawing.Point(0, 120)
        Me.btnvtasDeb.Name = "btnvtasDeb"
        Me.btnvtasDeb.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasDeb.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasDeb.TabIndex = 40
        Me.btnvtasDeb.Text = "      Débitos"
        Me.btnvtasDeb.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasDeb.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasDeb.UseVisualStyleBackColor = True
        '
        'btnvtasCre
        '
        Me.btnvtasCre.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasCre.FlatAppearance.BorderSize = 0
        Me.btnvtasCre.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasCre.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasCre.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasCre.IconChar = FontAwesome.Sharp.IconChar.Percentage
        Me.btnvtasCre.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasCre.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasCre.IconSize = 32
        Me.btnvtasCre.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasCre.Location = New System.Drawing.Point(0, 80)
        Me.btnvtasCre.Name = "btnvtasCre"
        Me.btnvtasCre.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasCre.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasCre.TabIndex = 39
        Me.btnvtasCre.Text = "      Créditos"
        Me.btnvtasCre.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasCre.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasCre.UseVisualStyleBackColor = True
        '
        'btnvtasOpr
        '
        Me.btnvtasOpr.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasOpr.FlatAppearance.BorderSize = 0
        Me.btnvtasOpr.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasOpr.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasOpr.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasOpr.IconChar = FontAwesome.Sharp.IconChar.ClipboardList
        Me.btnvtasOpr.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasOpr.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasOpr.IconSize = 32
        Me.btnvtasOpr.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasOpr.Location = New System.Drawing.Point(0, 40)
        Me.btnvtasOpr.Name = "btnvtasOpr"
        Me.btnvtasOpr.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasOpr.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasOpr.TabIndex = 38
        Me.btnvtasOpr.Text = "      Operaciones"
        Me.btnvtasOpr.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasOpr.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasOpr.UseVisualStyleBackColor = True
        '
        'btnvtasReg
        '
        Me.btnvtasReg.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnvtasReg.FlatAppearance.BorderSize = 0
        Me.btnvtasReg.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnvtasReg.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnvtasReg.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnvtasReg.IconChar = FontAwesome.Sharp.IconChar.Edit
        Me.btnvtasReg.IconColor = System.Drawing.Color.Gainsboro
        Me.btnvtasReg.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnvtasReg.IconSize = 32
        Me.btnvtasReg.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasReg.Location = New System.Drawing.Point(0, 0)
        Me.btnvtasReg.Name = "btnvtasReg"
        Me.btnvtasReg.Padding = New System.Windows.Forms.Padding(35, 0, 20, 0)
        Me.btnvtasReg.Size = New System.Drawing.Size(203, 40)
        Me.btnvtasReg.TabIndex = 37
        Me.btnvtasReg.Text = "      Registros"
        Me.btnvtasReg.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnvtasReg.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnvtasReg.UseVisualStyleBackColor = True
        '
        'btnVentas
        '
        Me.btnVentas.BackColor = System.Drawing.Color.FromArgb(CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer), CType(CType(67, Byte), Integer))
        Me.btnVentas.Dock = System.Windows.Forms.DockStyle.Top
        Me.btnVentas.FlatAppearance.BorderSize = 0
        Me.btnVentas.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnVentas.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnVentas.ForeColor = System.Drawing.Color.Gainsboro
        Me.btnVentas.IconChar = FontAwesome.Sharp.IconChar.ChartBar
        Me.btnVentas.IconColor = System.Drawing.Color.Gainsboro
        Me.btnVentas.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnVentas.IconSize = 32
        Me.btnVentas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVentas.Location = New System.Drawing.Point(0, 0)
        Me.btnVentas.Name = "btnVentas"
        Me.btnVentas.Padding = New System.Windows.Forms.Padding(10, 0, 20, 0)
        Me.btnVentas.Size = New System.Drawing.Size(203, 60)
        Me.btnVentas.TabIndex = 25
        Me.btnVentas.Text = "      Ventas"
        Me.btnVentas.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btnVentas.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.btnVentas.UseVisualStyleBackColor = False
        '
        'pnlEscritorio
        '
        Me.pnlEscritorio.BackColor = System.Drawing.Color.FromArgb(CType(CType(253, Byte), Integer), CType(CType(215, Byte), Integer), CType(CType(54, Byte), Integer))
        Me.pnlEscritorio.BackgroundImage = CType(resources.GetObject("pnlEscritorio.BackgroundImage"), System.Drawing.Image)
        Me.pnlEscritorio.Controls.Add(Me.PictureBox2)
        Me.pnlEscritorio.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlEscritorio.Location = New System.Drawing.Point(220, 70)
        Me.pnlEscritorio.Name = "pnlEscritorio"
        Me.pnlEscritorio.Size = New System.Drawing.Size(782, 550)
        Me.pnlEscritorio.TabIndex = 6
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(525, 418)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(250, 125)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox2.TabIndex = 1
        Me.PictureBox2.TabStop = False
        '
        'FrmPrincipal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(1002, 650)
        Me.Controls.Add(Me.pnlEscritorio)
        Me.Controls.Add(Me.pnlMenu)
        Me.Controls.Add(Me.pnlStatus)
        Me.Controls.Add(Me.pnlBarra)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None
        Me.MaximumSize = New System.Drawing.Size(1002, 650)
        Me.Name = "FrmPrincipal"
        Me.Text = "Form1"
        Me.pnlBarra.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.pnlMenu.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.pnlSistemas.ResumeLayout(False)
        Me.pnlReportes.ResumeLayout(False)
        Me.pnlNomina.ResumeLayout(False)
        Me.pnladministracion.ResumeLayout(False)
        Me.PnlInventario.ResumeLayout(False)
        Me.pnlCompras.ResumeLayout(False)
        Me.pnlVentas.ResumeLayout(False)
        Me.pnlEscritorio.ResumeLayout(False)
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents pnlBarra As Panel
    Friend WithEvents btnPerfil As FontAwesome.Sharp.IconButton
    Friend WithEvents btnMaximizar As FontAwesome.Sharp.IconButton
    Friend WithEvents btnRestaurar As FontAwesome.Sharp.IconButton
    Friend WithEvents btnMinimizar As FontAwesome.Sharp.IconButton
    Friend WithEvents btnCerrar As FontAwesome.Sharp.IconButton
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents pnlStatus As Panel
    Friend WithEvents pnlMenu As Panel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents btnHome As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSalir As FontAwesome.Sharp.IconButton
    Friend WithEvents pnlSistemas As Panel
    Friend WithEvents btnSisResp As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisEjCon As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisEjCo As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisEjAc As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisImpF As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisPara As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisOpcS As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisPfac As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisCEmp As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisEmp As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisCla As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSisUsr As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSistema As FontAwesome.Sharp.IconButton
    Friend WithEvents pnlReportes As Panel
    Friend WithEvents btnRepAdm As FontAwesome.Sharp.IconButton
    Friend WithEvents btnRepInv As FontAwesome.Sharp.IconButton
    Friend WithEvents btnRepCom As FontAwesome.Sharp.IconButton
    Friend WithEvents btnRepVtas As FontAwesome.Sharp.IconButton
    Friend WithEvents btnReportes As FontAwesome.Sharp.IconButton
    Friend WithEvents pnlNomina As Panel
    Friend WithEvents btnNomRet As FontAwesome.Sharp.IconButton
    Friend WithEvents btnNomOpr As FontAwesome.Sharp.IconButton
    Friend WithEvents btnNomReg As FontAwesome.Sharp.IconButton
    Friend WithEvents btnNomina As FontAwesome.Sharp.IconButton
    Friend WithEvents pnladministracion As Panel
    Friend WithEvents btnImpAdmin As FontAwesome.Sharp.IconButton
    Friend WithEvents btnOprAdmin As FontAwesome.Sharp.IconButton
    Friend WithEvents btnRegAdmin As FontAwesome.Sharp.IconButton
    Friend WithEvents BtnAdministracion As FontAwesome.Sharp.IconButton
    Friend WithEvents PnlInventario As Panel
    Friend WithEvents btnOperaciones As FontAwesome.Sharp.IconButton
    Friend WithEvents btnRegistros As FontAwesome.Sharp.IconButton
    Friend WithEvents btnInventario As FontAwesome.Sharp.IconButton
    Friend WithEvents pnlCompras As Panel
    Friend WithEvents btnCxP As FontAwesome.Sharp.IconButton
    Friend WithEvents btnOprCompras As FontAwesome.Sharp.IconButton
    Friend WithEvents btnRegCompras As FontAwesome.Sharp.IconButton
    Friend WithEvents btnCompras As FontAwesome.Sharp.IconButton
    Friend WithEvents pnlVentas As Panel
    Friend WithEvents btnvtasCxC As FontAwesome.Sharp.IconButton
    Friend WithEvents btnvtasTra As FontAwesome.Sharp.IconButton
    Friend WithEvents btnvtasCon As FontAwesome.Sharp.IconButton
    Friend WithEvents btnvtasVen As FontAwesome.Sharp.IconButton
    Friend WithEvents btnvtasDeb As FontAwesome.Sharp.IconButton
    Friend WithEvents btnvtasCre As FontAwesome.Sharp.IconButton
    Friend WithEvents btnvtasOpr As FontAwesome.Sharp.IconButton
    Friend WithEvents btnvtasReg As FontAwesome.Sharp.IconButton
    Friend WithEvents btnVentas As FontAwesome.Sharp.IconButton
    Friend WithEvents pnlEscritorio As Panel
    Friend WithEvents PictureBox2 As PictureBox
End Class
