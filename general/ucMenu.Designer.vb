﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ucMenu
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.btnLimpiar = New FontAwesome.Sharp.IconButton()
        Me.btnBuscar = New FontAwesome.Sharp.IconButton()
        Me.btnImprimir = New FontAwesome.Sharp.IconButton()
        Me.btnSeparador = New FontAwesome.Sharp.IconButton()
        Me.btnGuardar = New FontAwesome.Sharp.IconButton()
        Me.btnEliminar = New FontAwesome.Sharp.IconButton()
        Me.btnEdit = New FontAwesome.Sharp.IconButton()
        Me.SuspendLayout()
        '
        'btnLimpiar
        '
        Me.btnLimpiar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnLimpiar.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnLimpiar.FlatAppearance.BorderSize = 0
        Me.btnLimpiar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLimpiar.ForeColor = System.Drawing.Color.Transparent
        Me.btnLimpiar.IconChar = FontAwesome.Sharp.IconChar.Brush
        Me.btnLimpiar.IconColor = System.Drawing.Color.White
        Me.btnLimpiar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnLimpiar.IconSize = 29
        Me.btnLimpiar.Location = New System.Drawing.Point(192, 0)
        Me.btnLimpiar.Name = "btnLimpiar"
        Me.btnLimpiar.Size = New System.Drawing.Size(35, 38)
        Me.btnLimpiar.TabIndex = 76
        Me.btnLimpiar.Tag = "DashBoard"
        Me.btnLimpiar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnLimpiar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnLimpiar.UseVisualStyleBackColor = True
        '
        'btnBuscar
        '
        Me.btnBuscar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnBuscar.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnBuscar.FlatAppearance.BorderSize = 0
        Me.btnBuscar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnBuscar.ForeColor = System.Drawing.Color.Transparent
        Me.btnBuscar.IconChar = FontAwesome.Sharp.IconChar.Search
        Me.btnBuscar.IconColor = System.Drawing.Color.White
        Me.btnBuscar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnBuscar.IconSize = 32
        Me.btnBuscar.Location = New System.Drawing.Point(157, 0)
        Me.btnBuscar.Name = "btnBuscar"
        Me.btnBuscar.Size = New System.Drawing.Size(35, 38)
        Me.btnBuscar.TabIndex = 75
        Me.btnBuscar.Tag = "DashBoard"
        Me.btnBuscar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnBuscar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnBuscar.UseVisualStyleBackColor = True
        '
        'btnImprimir
        '
        Me.btnImprimir.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnImprimir.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnImprimir.FlatAppearance.BorderSize = 0
        Me.btnImprimir.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnImprimir.ForeColor = System.Drawing.Color.Transparent
        Me.btnImprimir.IconChar = FontAwesome.Sharp.IconChar.Print
        Me.btnImprimir.IconColor = System.Drawing.Color.White
        Me.btnImprimir.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnImprimir.IconSize = 32
        Me.btnImprimir.Location = New System.Drawing.Point(119, 0)
        Me.btnImprimir.Name = "btnImprimir"
        Me.btnImprimir.Size = New System.Drawing.Size(38, 38)
        Me.btnImprimir.TabIndex = 74
        Me.btnImprimir.Tag = "DashBoard"
        Me.btnImprimir.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnImprimir.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnImprimir.UseVisualStyleBackColor = True
        '
        'btnSeparador
        '
        Me.btnSeparador.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnSeparador.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnSeparador.Enabled = False
        Me.btnSeparador.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.btnSeparador.FlatAppearance.BorderSize = 0
        Me.btnSeparador.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.btnSeparador.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.btnSeparador.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSeparador.ForeColor = System.Drawing.Color.Transparent
        Me.btnSeparador.IconChar = FontAwesome.Sharp.IconChar.GripLinesVertical
        Me.btnSeparador.IconColor = System.Drawing.Color.White
        Me.btnSeparador.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnSeparador.IconSize = 32
        Me.btnSeparador.Location = New System.Drawing.Point(105, 0)
        Me.btnSeparador.Margin = New System.Windows.Forms.Padding(3, 3, 0, 3)
        Me.btnSeparador.Name = "btnSeparador"
        Me.btnSeparador.Padding = New System.Windows.Forms.Padding(10, 3, 0, 0)
        Me.btnSeparador.Size = New System.Drawing.Size(14, 38)
        Me.btnSeparador.TabIndex = 73
        Me.btnSeparador.Tag = "DashBoard"
        Me.btnSeparador.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSeparador.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnSeparador.UseVisualStyleBackColor = True
        '
        'btnGuardar
        '
        Me.btnGuardar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnGuardar.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnGuardar.FlatAppearance.BorderSize = 0
        Me.btnGuardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGuardar.ForeColor = System.Drawing.Color.Transparent
        Me.btnGuardar.IconChar = FontAwesome.Sharp.IconChar.Save
        Me.btnGuardar.IconColor = System.Drawing.Color.White
        Me.btnGuardar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnGuardar.IconSize = 32
        Me.btnGuardar.Location = New System.Drawing.Point(70, 0)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(35, 38)
        Me.btnGuardar.TabIndex = 72
        Me.btnGuardar.Tag = "DashBoard"
        Me.btnGuardar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnGuardar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEliminar.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnEliminar.FlatAppearance.BorderSize = 0
        Me.btnEliminar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEliminar.ForeColor = System.Drawing.Color.Transparent
        Me.btnEliminar.IconChar = FontAwesome.Sharp.IconChar.Eraser
        Me.btnEliminar.IconColor = System.Drawing.Color.White
        Me.btnEliminar.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnEliminar.IconSize = 32
        Me.btnEliminar.Location = New System.Drawing.Point(35, 0)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(35, 38)
        Me.btnEliminar.TabIndex = 71
        Me.btnEliminar.Tag = "DashBoard"
        Me.btnEliminar.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEliminar.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'btnEdit
        '
        Me.btnEdit.Cursor = System.Windows.Forms.Cursors.Hand
        Me.btnEdit.Dock = System.Windows.Forms.DockStyle.Left
        Me.btnEdit.FlatAppearance.BorderSize = 0
        Me.btnEdit.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnEdit.ForeColor = System.Drawing.Color.Transparent
        Me.btnEdit.IconChar = FontAwesome.Sharp.IconChar.Edit
        Me.btnEdit.IconColor = System.Drawing.Color.White
        Me.btnEdit.IconFont = FontAwesome.Sharp.IconFont.[Auto]
        Me.btnEdit.IconSize = 32
        Me.btnEdit.Location = New System.Drawing.Point(0, 0)
        Me.btnEdit.Name = "btnEdit"
        Me.btnEdit.Size = New System.Drawing.Size(35, 38)
        Me.btnEdit.TabIndex = 70
        Me.btnEdit.Tag = "DashBoard"
        Me.btnEdit.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnEdit.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnEdit.UseVisualStyleBackColor = True
        '
        'ucMenu
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(32, Byte), Integer), CType(CType(39, Byte), Integer))
        Me.Controls.Add(Me.btnLimpiar)
        Me.Controls.Add(Me.btnBuscar)
        Me.Controls.Add(Me.btnImprimir)
        Me.Controls.Add(Me.btnSeparador)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.btnEliminar)
        Me.Controls.Add(Me.btnEdit)
        Me.MaximumSize = New System.Drawing.Size(0, 38)
        Me.MinimumSize = New System.Drawing.Size(233, 38)
        Me.Name = "ucMenu"
        Me.Size = New System.Drawing.Size(389, 38)
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents btnLimpiar As FontAwesome.Sharp.IconButton
    Friend WithEvents btnBuscar As FontAwesome.Sharp.IconButton
    Friend WithEvents btnImprimir As FontAwesome.Sharp.IconButton
    Friend WithEvents btnSeparador As FontAwesome.Sharp.IconButton
    Friend WithEvents btnGuardar As FontAwesome.Sharp.IconButton
    Friend WithEvents btnEliminar As FontAwesome.Sharp.IconButton
    Friend WithEvents btnEdit As FontAwesome.Sharp.IconButton
End Class
